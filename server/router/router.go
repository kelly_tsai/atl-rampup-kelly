package router

import (
	"encoding/json"
	"fmt"
	"net/http"

	"../models"

	"github.com/gorilla/mux"
)

const (
	region = "us-west-1"
	bucket = "atlrampup"
)

// Router defines multiple endpoints for localhost:8080
func Router() *mux.Router {
	router := mux.NewRouter()
	// Call GetCodebaseLogo when you make a call to localhost:8080/
	router.HandleFunc("/", GetCodebaseLogo).Methods(http.MethodGet)
	return router
}

// GetCodebaseLogo gets returns a json payload containing the URL for the club logo
func GetCodebaseLogo(w http.ResponseWriter, r *http.Request) {
	key := "codebaes.png"
	// Determine what you want to return to the webapp
	// In this example, we are returning an object that contains one field
	output := models.Payload{
		ImageURL: fmt.Sprintf("https://s3-%s.amazonaws.com/%s/%s", region, bucket, key),
	}

	// Set up headers to get around CORS
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)

	// Convert your struct to json and send it to React
	json.NewEncoder(w).Encode(output)
}
